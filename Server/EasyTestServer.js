const fs = require("fs");
const express = require('express');
const configFile = "./EasyTestServer.config";
var runner = require('./runner');

var app = express();

console.log("Starting EasyTestServer...");

if (!fs.existsSync(configFile)) {
	var defaultServerParameters = {
		dbServer: "",
		dbName: "",
		dbUser: "",
		dbPassword: "",
		dbConnectionStatus: "ok"
	}
	fs.writeFileSync(configFile, (JSON.stringify(defaultServerParameters)));
}

//fs.writeFileSync("./EasyTestServer.config", (JSON.stringify(serverParameters)));
console.log("Starting reading...");
var serverParameters = JSON.parse(fs.readFileSync(configFile, 'utf8'));

// setServerParameters
app.get("/setServerParameters", function (req, res) {
	console.log('setServerParameters');
	serverParameters.dbServer = req.query['server'];
	serverParameters.dbName = req.query['name'];
	serverParameters.dbUser = req.query['user'];
	serverParameters.dbPassword = req.query['password'];
	console.log(serverParameters);

	fs.writeFileSync(configFile, (JSON.stringify(serverParameters)));
	res.send("nok");
});

// getServerParameters
app.get("/getServerParameters", function (req, res) {
	console.log("getServerParameters");
	res.json(fs.readFileSync(configFile, 'utf8'));
});

// getSuites
app.get("/getSuites", function (req, res) {
	fs.readdir('./suites', function (err, files) {
		res.json('{ "testSuites":' + JSON.stringify(files) + '}');
	});
});

// getTestCases
app.get("/getTestCases", function (req, res) {
	var suiteName = './suites/' + req.query['suite'];
	fs.readdir(suiteName, function (err, files) {
		// remove .js from the filenames
		files.forEach(function (testCaseFile, index, currentArray) {
			currentArray[index] = testCaseFile.replace('.js', '');
		});
		res.json('{ "testCases":' + JSON.stringify(files) + '}');
	});
});

// getTestDescritpion
app.get("/getTestDescription", function (req, res) {
	var testFile = './suites/' + req.query['suite'] + '/' + req.query['test'];
	var testSpecification = require(testFile);
	var currentTest = new testSpecification();
	var returnObject = { 
		testName: currentTest.testName,
		testDescription: currentTest.testDescription
	};
	res.json(JSON.stringify(returnObject));
});

// getTestStatus
app.get("/getTestStatus", function (req, res) {
	var suiteName = req.query['suite'];
	var testName = req.query['test'];
	var statusFile = './log/' + suiteName + "/" + testName + ".status";
	console.log("Request: " + statusFile);
	if (fs.existsSync(statusFile)) {
		res.json(fs.readFileSync(statusFile, 'utf8'));
	} else {
		res.json('{"runTime":"","startTime":"","endTime":"","friendlyStartTime":"","testStatus":"UNKNOWN"}');
	}
});

// runTest
app.get("/runTest", function (req, res) {
	var suiteName = req.query['suite'];
	var testName = req.query['test'];
	var testServer = req.query['servername'];
	var testServerPath = req.query['serverpath'];
	var testFile = '../suites/' + suiteName + '/' + testName;
	console.log("Starting test: " + testFile + "URL: " + testServer + "/" + testServerPath);
	res.send("ok");
	res.end();
	currentRunner.addTestToQueue(testFile, "MANUAL RUN", testServer, testServerPath);
	currentRunner.execute(function() { console.log("kickstarting flow"); });
	});

// getTestLog
app.get("/getTestLog", function (req, res) {
	var suiteName = req.query['suite'];
	var pLogfile = req.query['logfile'];
	var statusFile = './log/' + suiteName + "/" + pLogfile;
	console.log("Logrequest" + statusFile);
	if (fs.existsSync(statusFile)) {
		res.sendFile(statusFile, {
			root : "."
		});
	} else {
		res.send("ERROR, Can't find file: " + statusFile);
	}
});

// getImage
app.get("/getImage", function (req, res) {
	var suiteName = req.query['suite'];
	var pLogfile = req.query['logfile'];
	var imgFile = './log/' + suiteName + "/" + pLogfile;
	console.log("Logimage: " + imgFile);
	if (fs.existsSync(imgFile)) {
		res.sendFile(imgFile, {
			root : "."
		});
	} else {
		res.sendFile("noscreenshot.jpg", {
			root : "./log"
		});
	}
});

/* serves main page */
app.get("/", function (req, res, next) {
	res.sendFile('./testdesk.html', {
		root : "../client"
	})
});

/* serves all the static files */
app.get(/^(.+)$/, function (req, res, next) {
	console.log('static file request : ' + req.params[0]);
	res.sendFile(req.params[0], {
		root : "../client"
	});
});

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});
console.log("Starting new runner...");
var currentRunner = new runner();
if (!fs.existsSync("./log/")) {
	fs.mkdirSync("./log/");
}

var port = process.env.PORT || 4564;

app.listen(port, function () {
	console.log("Listening on " + port);
});

