function runner() {

	const webdriver = require('selenium-webdriver');
	const SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
	const testCase = require('./templates/testCase'); //load testcase

	var flow = webdriver.promise.controlFlow(), num = 0, start = Date.now();
	var newTest;


	var listTests = [];

	this.addTestToQueue = function(pName, pRunBy, pServer, pApplication) {
console.log("Adding test to queue");
		var testToRun = new testCase(pName, pRunBy, pServer, pApplication, "pending");
		testToRun.setStatus("pending");
		listTests.push(testToRun);
	}
	
	this.execute = function(pFunction) {
		flow.execute(function () {
			pFunction();
		});
	};
console.log("New flow started...");
	var newFlow = webdriver.promise.controlFlow(), num = 0, start = Date.now();
	var first = true;
	var firstFlow = true;

	if(firstFlow) {
		firstFlow = false;
console.log("Defining idler...");

		flow.on("idle", function() {
			console.log("Accessing queue.....");

			if ( listTests.length !== 0 ) {
				console.log(listTests.length);
				
				newTest = listTests[0];
				listTests.shift();

				if (first) {	//first time attach eventhandler
				first = false;
console.log("Defining exceptionhandler...");

					newFlow.on('uncaughtException', function (err) {
						console.log("Handling exception: " + newTest.getName() +  err );
						newTest.handleError(err);
					});
				}

				console.log("Queue opens test: " + newTest.getName());

				newFlow.execute(function () {
					newTest.setup();
				});
				newFlow.execute(function () {
					newTest.execute();
				});
				newFlow.execute(function () {
					newTest.tearDown();
				});

			} 
		});
	}
}
module.exports = runner;		

