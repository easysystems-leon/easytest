// zenix-framework.js

module.exports = {
	absenceRequest : function (pFramework) {
		this.frameWork = pFramework;
		var driver = this.frameWork.testHandler.seleniumDriver;

		const webdriver = require('selenium-webdriver');
		const Wait = require('selenium-webdriver').Wait;
		const By = require('selenium-webdriver').By;
		const until = require('selenium-webdriver').until;
		const Key = require('selenium-webdriver').Key;

		this.descriptionFieldInput = function (pDescription) {
			driver.findElement(By.id('401')).sendKeys(pDescription);
		};

		this.HoursFieldInput = function (pHours) {
			driver.findElement(By.id('511')).click();
			this.WaitForSaved();
			driver.findElement(By.id('511')).clear();
			driver.findElement(By.id('511')).sendKeys(pHours);
		};

		this.TypeFieldInput = function (pType) {
			driver.findElement(By.xpath("//div/div/div/div/div/span")).click();
			this.WaitForSaved();
			driver.findElement(By.xpath("//control-generator/div/div/div/div/div/div/input")).sendKeys(pType + Key.ENTER);

			// move to hours field to save the line
			//driver.findElement(By.xpath("//control-generator/div/div/div/div/div/div/input")).sendKeys(Key.TAB);
			this.WaitForUpdated();
			};
			
		this.identifierRead = function() {
			return(driver.findElement(By.xpath("//form-control-label/div/label")).getText());
		}
		
		this.SubmitButtonClickAndApprove = function () {
			driver.findElement(By.xpath("//*[contains(text(), 'Submit')]")).click();
			driver.wait(until.elementLocated(By.xpath("//*[contains(text(), 'Confirm')]")), 10000, "Can't find confirmation dialog");
			driver.findElement(By.xpath("//*[contains(text(), 'Yes')]")).click();
		}
		
		this.WaitForSaved = function () {
			driver.wait(until.elementLocated(By.xpath("//*[contains(text(), 'ave')]")), 10000, "Waiting done");

			// In case we want to wait until the savedmessage dissapears
			driver.wait(until.stalenessOf(driver.findElement(By.xpath("//*[contains(text(), 'ave')]")), 80000, "Cant find saved message"));
		}

		this.WaitLong = function () {
			driver.wait(until.elementLocated(By.xpath("//*[contains(text(), 'Srwerewrqewuccess')]")), 10000, "Temporay wait");
		}

		this.WaitForUpdated = function () {
			driver.wait(until.elementLocated(By.xpath("//*[contains(text(), 'Success')]")), 10000, "Cant find updatedline message");
		}
	}
}
