// zenix-framework.js

module.exports = {
	/* Zenixsession controls overall Zenix functionality like
	logging on and role management
	 */
	zenixSession : function (testHandler) {
		this.testHandler = testHandler;
		this.serverName = this.testHandler.server;
		this.versionName = this.testHandler.application;
		this.url = 'http://' + this.serverName + '/' + this.versionName;

		const webdriver = require('selenium-webdriver');
		const Wait = require('selenium-webdriver').Wait;
		const By = require('selenium-webdriver').By;
		const until = require('selenium-webdriver').until;
		const dslRequest = require('./absence-request');
		const dslOverview = require('./absence-overview');

		var driver = this.testHandler.seleniumDriver;

		
		this.absenceRequest = new dslRequest.absenceRequest(this);
		this.absenceOverview = new dslOverview.absenceOverview(this);
		
		/* 	startSession
		Start de browser, logt in en wacht tot Zenix geladen is
		 */
		this.startSession = function () {
			try {
				this.testHandler.testLog("Starting Zenix session");

				/* AutoIT script opstarten om NTLM invoerscherm te omzeilen op een manier die
				voor alle browsers out of the box zou moeten werken
				 */
				var child_process = require('child_process');
				child_process.execFile(".\\externals\\inlogFirefox.exe", [], function (error, stdout, stderr) {
					if (error) {
						console.log(error);
					}
				});

				// open link and wait for pagetitle to show
				driver.get(this.url);
				driver.wait(until.titleIs('Zenix'), 50000);
			} catch (e) {
				this.testHandler.handleError(e);
			}
		};

		/* 	logon
		Logs on to Zenix using the given username and password
		 */
		this.logon = function (pUsername, pPassword) {
			this.testHandler.testLog("Logon");

			driver.wait(until.elementLocated(By.id('username-email')), 10000, "Can't find usernamefield");
			driver.findElement(By.id('username-email')).sendKeys(pUsername);

			driver.wait(until.elementLocated(By.id('password')), 10000, "Can't find passwordfield");
			driver.findElement(By.id('password')).sendKeys(pPassword);

			driver.wait(until.elementLocated(By.css("button.btn.btn-primary")), 10000, "Can't find logon button");
			driver.findElement(By.css("button.btn.btn-primary")).click();

			driver.wait(until.elementLocated(By.css("strong.username.ng-binding")), 10000, "Can't find mention of username");
		}

		/* 	logoff
		Logs the current user off
		 */
		this.logoff = function () {
			this.testHandler.testLog("Logoff");
			driver.wait(until.elementLocated(By.css("strong.username.ng-binding")), 10000, "Can't find logoff button");
			driver.findElement(By.css("strong.username.ng-binding")).click();
			driver.findElement(By.linkText("LOGOFF")).click();
		};

		this.navigationbarClick = function (pItem) {
			driver.wait(until.elementLocated(By.linkText("Absences")), 10000);
			driver.findElement(By.linkText("Absences")).click();
		};

		this.dashboardOpenClick = function () {
			driver.wait(until.elementLocated(By.css("svg.navigation-icon")), 10000);
			driver.findElement(By.css("svg.navigation-icon")).click();
		};

		this.dashboardAbsenceNewAbsenceClick = function () {
			driver.findElement(By.xpath("//*[contains(text(), 'New request')]")).click();
			driver.wait(until.elementLocated(By.xpath("//*[contains(text(), 'Delete')]")), 10000);
		};

		this.newAbsenceDeleteClick = function () {
			driver.wait(until.elementLocated(By.xpath("//*[contains(text(), 'Delete')]")), 10000);
			driver.findElement(By.xpath("//*[contains(text(), 'Delete')]")).click();
		};

		/* 	endSession
		Closes browser
		 */
		this.endSession = function () {
			//driver.quit();
			console.log("Session ended");
		};
	}
}
