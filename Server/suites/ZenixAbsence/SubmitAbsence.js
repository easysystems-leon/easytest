/* 	SubmitAbsence
29-01-2016 LK	First concept
 */
function testSpecification(parentFunc) {
	this.myParent = parentFunc;

	this.testName = "SubmitAbsence";
	this.testDescription = "Fills in an absence\nAsserts the status equals concept\nSubmits the absence\nAsserts the absencestatus is submitted";

	// testvariables
	var parUsername = "absUser1";
	var parPassword = "password";
	var absenceDescription = "Submitted request";
	var absenceType = "Day Off";
	var absenceNrHours = 4;

	var testSession;

	const tools = require('../../dsl/zenix-framework');

	//setup
	this.setup = function () {
		tools = require('../../dsl/zenix-framework');
		testSession = new tools.zenixSession(this.myParent);
		testSession.startSession();
		testSession.logon(parUsername, parPassword);
	}

	//test
	this.test = function () {
		// open the absence creation screen
		testSession.navigationbarClick("Absences");
		testSession.dashboardOpenClick();
		testSession.dashboardAbsenceNewAbsenceClick();

		// read the generated ID and fill in the required fields
		absenceID = testSession.absenceRequest.identifierRead();
		testSession.absenceRequest.descriptionFieldInput(absenceDescription);
		testSession.absenceRequest.HoursFieldInput(absenceNrHours);
		testSession.absenceRequest.TypeFieldInput(absenceType);
		testSession.absenceRequest.SubmitButtonClickAndApprove();

		// check the overview for the correct status 
		testSession.absenceOverview.absenceHeadingExists();
		testSession.absenceOverview.absenceExists(absenceID);
		//testSession.absenceOverview.absenceExistsDetail(absenceID, absenceNrHours, absenceDescription, "Submfsdfdsitted");

		//temporary longwait
		//testSession.absenceRequest.WaitLong();
		}

	//teardown
	this.tearDown = function () {
		testSession.logoff();
		testSession.endSession();
	}

};

module.exports = testSpecification;
