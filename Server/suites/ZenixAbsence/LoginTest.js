/* 	LoginTest
29-01-2016 LK	First concept
 */
function testSpecification(parentFunc) {
	this.myParent = parentFunc;

	this.testName = "LoginTest";
	this.testDescription = "This test logs in to Zenix.\nAssert that the username is displayed\nProceeds to log off.";

	// testvariables
	var parUsername = "admin";
	var parPassword = "password";
	var testSession;

	const tools = require('../../dsl/zenix-framework');

	//setup
	this.setup = function () {
		testSession = new tools.zenixSession(this.myParent);
		testSession.startSession();
	}

	//test
	this.test = function () {
		testSession.logon(parUsername, parPassword);
		testSession.logoff();
	}

	//teardown
	this.tearDown = function () {
		testSession.endSession();
	}
};

module.exports = testSpecification;

