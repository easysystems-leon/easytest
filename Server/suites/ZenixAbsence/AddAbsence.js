/* 	AddAbsence
29-01-2016 LK	First concept
 */
function testSpecification(parentFunc) {
	this.myParent = parentFunc;

	this.testName = "AddAbsence";
	this.testDescription = "This adds an absence, but immediately deletes it";

	// testvariables
	var parUsername = "admin";
	var parPassword = "password";
	var testSession;

	const tools = require('../../dsl/zenix-framework');

	//setup
	this.setup = function () {
		tools = require('../../dsl/zenix-framework');
		testSession = new tools.zenixSession(this.myParent);
		testSession.startSession();
		testSession.logon(parUsername, parPassword);
	}

	//test
	this.test = function () {
		testSession.navigationbarClick("Absences");
		testSession.dashboardOpenClick();
		testSession.dashboardAbsenceNewAbsenceClick();
		testSession.newAbsenceDeleteClick();
	}

	//teardown
	this.tearDown = function () {
		testSession.logoff();
		testSession.endSession();
	}

};

module.exports = testSpecification;
