;27012016 LK Eerste versie
;AUTOit Script tbv het kunnen invoeren van inlogcredentials automatische test Selenium
Inlog()

Func Inlog()
	;NTLM credentialschem afwachten
	If WinWait("Authenticatie vereist","", 30) Then
		WinActivate("Authenticatie vereist")

		;Hard gecodeerde inloggegeven invullen
		Send("ic-test\lkrol{TAB}{SHIFTDOWN}w{SHIFTUP}elkom{SHIFTDOWN}2{SHIFTUP}2016{TAB}{ENTER}")
	Else
		Exit
	EndIf
EndFunc
