/* 	LoginTest
29-01-2016 LK	First concept
 */

function testCase(testFile, pUser, pServer, pApplication, pNewStatus) {
	const fs = require('fs');
	const testSpecification = require(testFile);
	const webdriver = require('selenium-webdriver');
	const SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;

	var objTestState = {
		runTime : 0,
		logTime : 0,
		startTime : 0,
		endTime : 0,
		friendlyStartTime : "",
		testStatus : pNewStatus,
		logFile : ""
	};

	var logDir;
	var logFile;
	var logStatusFile;

	//this.seleniumDriver = "";
	//this.testFlow;

	this.user = pUser;
	this.server = pServer;
	this.application = pApplication;

	// FUNCTION getName: returns the testname
	this.getName = function () {
		return this.currentSpecification.testName;
	}

	// FUNCTION getSuite: returns the name of the current testsuite
	this.getSuite = function () {
		var suiteName = testFile;
		suiteName = suiteName.replace("../suites/", "");
		return suiteName.substring(0, suiteName.lastIndexOf("/"));
	}

	// FUNCTION getDescription: returns the testdescription
	this.getDescription = function () {
		return currentSpecification.testDescription;
	}

	//FUNCTION getStatus: gets the teststatus
	this.getStatus = function () {
		return objTestState.testStatus;
	}

	//FUNCTION setStatus: sets the teststatus
	this.setStatus = function (newStatus) {
		objTestState.testStatus = newStatus;
		this.saveStatus();
	}

	//FUNCTION setup: setup up the test
	this.setup = function () {
		objTestState.startTime = new Date().getTime();
		this.setStatus("run");
		this.seleniumDriver = new webdriver.Builder().forBrowser('firefox').build();
		this.testLog("<SETUP> " + this.getName() + " - " + logFile);
		try {
			this.currentSpecification.setup();
		} catch (e) {
			this.handleError("Fout bij setup: " + e)
		}
	}

	//FUNCTION execute: execute the actual test
	this.execute = function () {
		this.testLog("<START EXECUTION>");
		objTestState.logTime = new Date().getTime();
		if (this.getStatus() !== "nok") {
			// testspecific execution
			try {
				this.currentSpecification.test();
			} catch (e) {
				this.handleError("Fout bij testuitvoer" + e)
			}
		}
	}

	//FUNCTION tearDown: clean up niceley after testing to assume a known state
	this.tearDown = function () {
		objTestState.endTime = new Date().getTime();
		if	( objTestState.logTime !== 0  ) {
			objTestState.runTime = objTestState.endTime - objTestState.logTime;
		} else objTestState.runTime = 0;
		if(this.getStatus() !== "nok") {
			this.setStatus("ok");
		}
		this.testLog("<TEARDOWN> Runtime: " + objTestState.runTime + "ms");

		// testspecific teardown
		if (this.getStatus() === "ok" && this.getStatus() === "ok") {
			this.currentSpecification.tearDown();
		}
		this.storeResults();
		this.seleniumDriver.close();
	}

	function fDigits(number, nrOfDigits) {
		var returnString = (nrOfDigits == 3 && number < 100 ? "0" : "");
		return returnString + (number > 9 ? "" + number : "0" + number);
	};

	//FUNCTION testLog: outputs a logmessage
	this.testLog = function (logMessage) {

		var date = new Date();
		var logText = fDigits(date.getHours()) + ":" + fDigits(date.getMinutes()) + ":" + fDigits(date.getSeconds()) + fDigits(date.getMilliseconds(), 3) + "> " + logMessage;
		logText = logText.replace('\n', ' - ').replace('\r', '');

		console.log(logText);
		try {
			fs.appendFileSync(logFile, logText + '\r\n');
		} catch (err) {
			console.log("<ERROR> Can't write to logfile " + logFile + " Errortext: " + err);
		}
	}

	//FUNCTION handleError: process error encountered during one of the testphases
	this.handleError = function (error) {
		this.testLog("<ERROR> " + error);
		this.setStatus("nok");
		try {
			this.seleniumDriver.takeScreenshot().then(function (data) {
				var captureImage = logFile.replace(".log", ".png");

				fs.writeFile(captureImage, data.replace(/^data:image\/png;base64,/, ''), 'base64', function (err) {
					console.log("Error saving screenshot");
					//this.testLog("<ERROR> Error saving screenshot: " + err);
				}, function () {
					console.log("Saved screenshot");
					//this.testLog("<SCREENSHOT>" + captureImage);
				});
			}, function () {
				console.log("Error making screenshot");
				//this.testLog("<ERROR> Error while making screenshot: " + error);
			});

		} catch (e) {
			this.testLog("<ERROR> Can't make screenshot: " + e);
		};
		this.tearDown();
	}

	//FUNCTION storeResults: Saves the test exection results in a remote database
	this.storeResults = function () {
		const dbConnection = require('./dbAccess'); //load testcase

		var tsStart = new Date(objTestState.startTime);
		var tsString = tsStart.getFullYear() + "-" + (tsStart.getMonth() ) + "-" + tsStart.getDate() + " " + tsStart.getHours() + ":" + tsStart.getMinutes() + ":" + tsStart.getSeconds();
		var newConnection = new dbConnection();
		newConnection.saveLogging(this.getSuite(), this.getName(), objTestState.runTime, this.getStatus(), tsString, this.server, this.application, this.user);

		this.saveStatus();
	}

	//FUNCTION saveStatus: updates the  .status file of this test in de logging directory
	this.saveStatus = function () {
		var tsStart = new Date(objTestState.startTime);
		
		if	(objTestState.startTime != 0 ) {
			objTestState.friendlyStartTime = tsStart.getDate() + "-" + (tsStart.getMonth() + 1) + "-" + tsStart.getFullYear() + " " + tsStart.getHours() + ":" + fDigits(tsStart.getMinutes(),2) + ":" + fDigits(tsStart.getSeconds(),2); ;
		} else {
			objTestState.friendlyStartTime = "";
			objTestState.runTime = ""; 
		}
		fs.writeFileSync(logStatusFile, (JSON.stringify(objTestState)));
	}

	// testcase declaration
	this.currentSpecification = new testSpecification(this);
	logDir = "./log/" + this.getSuite() + "/";
	// probably shouldn't happen here
	if (!fs.existsSync(logDir)) {
		fs.mkdirSync(logDir);
	}
	this.testRunID = this.getName() + new Date().getTime();

	objTestState.logFile = this.testRunID + ".log";
	logFile = logDir + this.testRunID + ".log";

	logStatusFile = logDir + this.getName() + ".status";

};
module.exports = testCase;
