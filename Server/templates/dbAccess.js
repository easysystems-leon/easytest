function dbConnection(pUser, pPass, pServer) {
	const configFile = "./EasyTestServer.config";
	const fs = require("fs");
	const Connection = require('tedious').Connection;
	var connection;
	const Request = require('tedious').Request;
	const TYPES = require('tedious').TYPES;
	var config;

	var serverParameters = JSON.parse(fs.readFileSync(configFile, 'utf8'));
	setConnectionPars();

	function setConnectionPars(pUser, pPass, pServer) {
		config = {
			userName : serverParameters.dbUser,
			password : serverParameters.dbPassword,
			server :  serverParameters.dbServer,
			options : {
				encrypt : false,
				database : serverParameters.dbName
			}
		};
	}

	this.connect = function () {
		connection = new Connection(config);
		connection.on('connect', function (err) {
			console.log("Connected");
		});
	}

	this.close = function () {
		connection.close();
	}

	this.saveLogging = function (pSuite, pTest, pRunTime, pStatus, pStartTime, pServer, pApplication, pRunBy) {
		var request;
		connection = new Connection(config);
		connection.on('connect', function (err) {
			if (err) {
				console.log("Can't make databaseconnection" + err);
			} else {

				console.log("Connected");

				// prepare request
				request = new Request("INSERT INTO TestLog(Suite, Testname, Runtime, Status, StartTime, Server, Application, RunBy) VALUES (@Suite, @Testname, @Runtime, @Status, @StartTime, @Server, @Application, @RunBy);",
						function (err) {
						if (err) {
							console.log(err);
						}
					});
				request.addParameter('Suite', TYPES.NVarChar, pSuite);
				request.addParameter('Testname', TYPES.NVarChar, pTest);
				request.addParameter('Runtime', TYPES.Numeric, pRunTime);
				request.addParameter('Status', TYPES.NVarChar, pStatus);
				request.addParameter('StartTime', TYPES.NVarChar, pStartTime);
				request.addParameter('Server', TYPES.NVarChar, pServer);
				request.addParameter('Application', TYPES.NVarChar, pApplication);
				request.addParameter('RunBy', TYPES.NVarChar, pRunBy);
				connection.execSql(request);
				connection.close()
			}
		});
	}
}
module.exports = dbConnection;
