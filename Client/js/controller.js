/*global $, global angular */
var app = angular.module('myApp', []);

/* ANGULAR Controller: ngTestSuites

 */
app.controller('ngTestSuites', function ($scope, $http, $timeout) {
	"use strict";

	var testSuites = [];
	var testCases = [];
	var currentSuite = "";

	// Function to make a runtime in ms readable
	$scope.makeFriendlyRunTime = function (pRunTime) {

		var number;
		if(pRunTime >= 3600000) {
			pRunTime = pRunTime / 3600000;
			return (pRunTime.toFixed(2) + " hours");
		}
		if(pRunTime >= 60000) {
			pRunTime = pRunTime / 60000;
			return (pRunTime.toFixed(2) + " minutes");
		}
		if(pRunTime >= 1000) {
			pRunTime = pRunTime / 1000;
			return (pRunTime.toFixed(2) + " seconds");
		}
		if(pRunTime > 0) {
			return (pRunTime + " milliseconds");
		}
		return "";
	}

	$scope.backendServer = localStorage.getItem("Easytest.server");
	if ($scope.backendServer === null || $scope.backendServer === "") {
		$scope.backendServer = "http://localhost";
	}

	$scope.backendPort = 4564;
	//	$scope.backendPort = localStorage.getItem("Easytest.port");
/*	if ($scope.backendPort === null || $scope.backendPort == "") {
		$scope.backendPort = "80";
	}*/

	if ($scope.backendPort === "80")
		var backendUrl = $scope.backendServer;
	else
		var backendUrl = $scope.backendServer + ":" + $scope.backendPort;

	// Function to replicate setInterval using $timeout service.
	$scope.intervalFunction = function () {
		$timeout(function () {
			for (var i = 0; i < $scope.testCases.length; i++) {
				let currentTest = $scope.testCases[i].name;

				if ($scope.testCases[i].status === "run" || $scope.testCases[i].status === "pending")
					// get teststatus
					$.ajax({
						type : "GET",
						data : 'suite=' + currentSuite + '&test=' + currentTest,
						contentType : "application/json; charset=utf-8",
						async : false,
						cache : false,
						url : backendUrl + "/getTestStatus",
						error : function (jqXhr, err, errorMessage) {
							console.log(err);
						},
						success : function (data2, status) {
							if (status === "success") {
								try {
									let jsonTestStatus = JSON.parse(data2);
									if ($scope.testCases[i].status != jsonTestStatus.testStatus) {
										$scope.testCases[i].status = jsonTestStatus.testStatus;
										$scope.testCases[i].lastRun = jsonTestStatus.friendlyStartTime;
										$scope.testCases[i].runTime = jsonTestStatus.runTime;
										$scope.testCases[i].logFile = jsonTestStatus.logFile;
									}
								} catch (err) {
									console.log(err);
								}
							}
						}
					});
			}
			$scope.intervalFunction();
		}, 8000);
	};

	// Kick off the interval
	$scope.intervalFunction();

	//$interval(getTestCases, 10000, 10, false);

	$scope.settingsZichtbaarheid = function (element) {
		if (element === "Settings") {
			return (currentSuite === "Settings") ? "divZichtbaar" : "divOnzichtbaar";
		} else {
			return (currentSuite === "Settings") ? "divOnzichtbaar" : "divZichtbaar";
		}
	};

	$scope.showTestLog = function (pSuite, pTestlog) {

		$.ajax({
			type : "GET",
			data : "",
			contentType : "application/json; charset=utf-8",
			async : false,
			cache : false,
			url : backendUrl + "/getTestLog?suite=" + pSuite + "&logfile=" + pTestlog,
			error : function (jqXhr, err, errorMessage) {
				console.log(err);
			},
			success : function (data, status) {
				if (status === "success") {
					try {
						//var htmlFormattedLog;
						var textStr = data;
						var htmlText = "<HTML>";

						htmlText += "<head>";
						htmlText += '<link rel="stylesheet" type="text/css" href="css/popup.css">';
						htmlText += '</head>';
						htmlText += '<body>';
						htmlText += '<h1>Testlog</h1>';
						htmlText += '<div id="testLog">';

						textStr = textStr.split("\n");
						textStr.forEach(function (tekst) {
							if (tekst.indexOf("ERROR") > 0) {
								htmlText += '<span class="error">' + tekst + '</span><BR>';
							} else {
								htmlText += tekst + "<BR>";
							}
						});
						htmlText += "<img src='" + backendUrl + "/getImage?suite=" + pSuite + "&logfile=" + pTestlog.replace(".log", ".png") + "'>";
						htmlText += '</div>';
						htmlText += '</body>';
						var myWindow = window.open("", "Logging", "width=800, height=670, toolbar=no, location=no, menubar=no, scrollbars=no, directories=no, location=no");
						myWindow.document.write(htmlText);

					} catch (err) {
						console.log(err);
					}
				}
			}
		});
	}
	$scope.selectSuite = function (selectedSuite) {
		currentSuite = selectedSuite;
		if (selectedSuite !== "Settings") {
			$scope.testCases = getTestCases(selectedSuite);
			$scope.suiteUrl = localStorage.getItem("Easytest.url." + currentSuite);
			$scope.suitePath = localStorage.getItem("Easytest.path." + currentSuite);
		} else {
			$scope.testCases = [];
			$scope.settingsZichtbaarheid();
		}
	};
	$scope.getCurrentSuite = function () {
		return currentSuite;
	};

	$scope.runAllTests = function () {
		for (var i = 0; i < $scope.testCases.length; i++) {
			$scope.runTest(currentSuite, $scope.testCases[i].name, $scope.testCases[i].status);
		}
	}

	$scope.runTest = function (pSuite, pTest, pStatus) {
		setTestStatus(pTest, "pending");
		if ((pStatus != "run") && (pStatus != "pending")) {
			$.ajax({
				type : "GET",
				data : "",
				async : true,
				cache : false,
				url : backendUrl + "/runtest?suite=" + pSuite + "&test=" + pTest + "&servername=" + $scope.suiteUrl + "&serverpath=" + $scope.suitePath,
				error : function (jqXhr, err, errorMessage) {
					console.log(err)
				},
				success : function (data, status) {
					setTestStatus(pTest, "pending");
					//alert(data);
				}
			});
		}
	}

	/* Object: testSuite
	Bevat alle gegevens om een doel weer te geven op de blogpagina
	 */
	function testSuite(naam, geselecteerd) {
		"use strict";

		this.naam = naam;
		//this.storedUrl = pUrl;
		this.geselecteerd = function () {
			return (this.naam === currentSuite)
		};
	};
	$scope.saveServerSettings = function () {
		setBackend();
		// get teststatus
		$.ajax({
			type : "GET",
			data : 'server=' + $scope.dbServer + '&name=' + $scope.dbName + '&user=' + $scope.dbUser + '&password=' + $scope.dbPassword,
			async : false,
			cache : false,
			url : backendUrl + "/setServerParameters",
			error : function (jqXhr, err, errorMessage) {
				console.log(err);
			},
			success : function (data, status) {
				if (status === "success") {
					$scope.dbConnectionStatus = data;
				} else
					$scope.dbConnectionStatus = "nok";
			}
		});
	};

	$scope.setSuiteUrl = function () {
		localStorage.setItem("Easytest.url." + currentSuite, $scope.suiteUrl);
		localStorage.setItem("Easytest.path." + currentSuite, $scope.suitePath);
	};

	function setBackend() {
		localStorage.setItem("Easytest.server", $scope.backendServer);
		localStorage.setItem("Easytest.port", $scope.backendPort);
	};

	function sessionSetting(easyServer, easyPort, applicationUnderTest) {
		this.easyServer = easyServer;
		this.easyPort = easyPort;
		this.applicationUnderTest = applicationUnderTest;
	}

	$scope.getNrStatus = function (pStatus) {
		let aantalPending = 0;
		for (var i = 0; i < $scope.testCases.length; i++) {
			if ($scope.testCases[i].status === pStatus) {
				aantalPending++
			}
		};
		return aantalPending;
	}

	function setTestStatus(pName, pStatus) {
		for (var i = 0; i < $scope.testCases.length; i++) {
			if ($scope.testCases[i].name === pName) {
				$scope.testCases[i].status = pStatus;
			}
		};
	}

	function testCase(pName, pDescription, pStatus, pLastRun, pRunTime, pLogFile) {
		"use strict";

		this.name = pName;
		this.description = pDescription;
		this.status = pStatus;
		this.lastRun = pLastRun;
		this.runTime = pRunTime;
		this.logFile = pLogFile;
		this.getStatusIcon = function () {
			switch (this.status.toLowerCase()) {
			case "ok":
				return "./images/ok.png";
			case "nok":
				return "./images/nok.png";
			case "run":
				return "./images/running.gif";
			case "unknown":
				return "./images/unknown.png";
			case "pending":
				return "./images/waiting.gif";
			}

			return "./images/unknown.gif";
		};
	};

	function getTestCases(suiteName) {
		var returnArray = [];
		$.ajax({
			type : "GET",
			data : "suite=" + suiteName,
			contentType : "application/json; charset=utf-8",
			async : false,
			cache : false,
			url : backendUrl + "/getTestcases",
			error : function (jqXhr, err, errorMessage) {
				alert(errorMessage)
			},
			success : function (data, status) {
				if (status === "success") {
					try {
						let jsonResponse = JSON.parse(data);
						$.each(jsonResponse.testCases, function (key, jsonTestName) {
							var currentName = "default";
							var currentDescription = "default";

							// get testname and description
							$.ajax({
								type : "GET",
								data : 'suite=' + suiteName + '&test=' + jsonTestName,
								contentType : "application/json; charset=utf-8",
								async : false,
								cache : false,
								url : backendUrl + "/getTestDescription",
								error : function (jqXhr, err, errorMessage) {
									console.log(err);
								},
								success : function (data2, status) {
									if (status === "success") {
										try {
											let jsonTestStatus = JSON.parse(data2);
											currentName = jsonTestStatus.testName;
											currentDescription = jsonTestStatus.testDescription;
										} catch (err) {
											console.log(err);
										}
									}
								}
							});
							// get teststatus
							$.ajax({
								type : "GET",
								data : 'suite=' + suiteName + '&test=' + jsonTestName,
								contentType : "application/json; charset=utf-8",
								async : false,
								cache : false,
								url : backendUrl + "/getTestStatus",
								error : function (jqXhr, err, errorMessage) {
									console.log(err);
								},
								success : function (data2, status) {
									if (status === "success") {
										try {
											let jsonTestStatus = JSON.parse(data2);
											var newTestcase = new testCase(currentName, currentDescription, jsonTestStatus.testStatus, jsonTestStatus.friendlyStartTime, jsonTestStatus.runTime, jsonTestStatus.logFile);
											returnArray.push(newTestcase);
										} catch (err) {
											console.log(err);
										}
									}
								}
							});

						})
					} catch (err) {
						console.log(err);
					}
				}
			}
		});
		return $(returnArray);
	}

	// inlezen suites
	$.ajax({
		type : "GET",
		data : "",
		contentType : "application/json; charset=utf-8",
		async : false,
		cache : false,
		url : backendUrl + "/getsuites",
		error : function (jqXhr, err, errorMessage) {
			alert(errorMessage)
		},
		success : function (data, status) {
			if (status === "success") {
				try {
					let jsonResponse = JSON.parse(data);
					$.each(jsonResponse.testSuites, function (key, jsonSuite) {
						if (currentSuite === "")
							currentSuite = jsonSuite;
						var newSuite = new testSuite(jsonSuite);
						testSuites.push(newSuite);
					});
					$scope.testSuites = $(testSuites);
				} catch (err) {
					console.log(err);
				}
				$scope.selectSuite(currentSuite);
			}
		}
	});

	// inlezen serversettings
	$.ajax({
		type : "GET",
		data : "",
		contentType : "application/json; charset=utf-8",
		async : false,
		cache : false,
		url : backendUrl + "/getServerParameters",
		error : function (jqXhr, err, errorMessage) {
			console.log(err)
		},
		success : function (data, status) {
			if (status === "success") {
				try {
					let jsonResponse = JSON.parse(data);
					$scope.dbServer = jsonResponse.dbServer;
					$scope.dbName = jsonResponse.dbName;
					$scope.dbUser = jsonResponse.dbUser;
					$scope.dbPassword = jsonResponse.dbPassword;
					$scope.dbConnectionStatus = jsonResponse.dbConnectionStatus;
				} catch (err) {
					console.log(err);
				}
			}
		}
	});

});
